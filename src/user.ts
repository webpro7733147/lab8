import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role";
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const roleRepository = AppDataSource.getRepository(Role)
    const adminRole = await roleRepository.findOneBy({ id: 1 })
    const userRole = await roleRepository.findOneBy({ id: 2 })


    const userRepository = AppDataSource.getRepository(User);
    await userRepository.clear()
    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 1;
    user.email = "admin@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = []
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Inserting a new user into the database...");
    await userRepository.save(user)



    var user = new User();
    user.id = 2;
    user.email = "user1@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the database...");
    await userRepository.save(user)


    var user = new User();
    user.id = 3;
    user.email = "user2@email.com";
    user.password = "Pass@1234";
    user.gender = "female";
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the database...");
    await userRepository.save(user)

    const users = await userRepository.find({ relations: { roles: true } })
    console.log(JSON.stringify(users,null,2));

    const roles = await roleRepository.find({ relations: { users: true } })
    console.log(JSON.stringify(roles,null,2));
    
}).catch(error => console.log(error))
